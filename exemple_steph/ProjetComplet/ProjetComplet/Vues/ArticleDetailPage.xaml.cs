﻿using AutoMapper;
using CommonServiceLocator;
using ProjetComplet.Dtos;
using ProjetComplet.Services;
using ProjetComplet.VuesModeles;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjetComplet.Vues
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ArticleDetailPage : ContentPage
    {
        public ArticleDetailPage(ArticleDto article)
        {
            InitializeComponent();

            BindingContext = ServiceLocator.Current.GetInstance<IMapper>().Map(article,
                ServiceLocator.Current.GetInstance<ArticleDetailVueModele>());
        }

        private void ToolbarItem_Activated(object sender, System.EventArgs e)
        {
            var vm = (ArticleDetailVueModele)BindingContext;

            if (vm.SauvegarderCommand.CanExecute(null))
            {
                vm.SauvegarderCommand.Execute(null);

                Navigation.PopAsync();
            }
            else
                ServiceLocator.Current.GetInstance<IPageService>().AfficherMessageErreur("Le nom de l'article ne peut pas être vide");
        }
    }
}