﻿using CommonServiceLocator;
using ProjetComplet.Dtos;
using ProjetComplet.VuesModeles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjetComplet.Vues
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ClientsPage : ContentPage
    {
        public ClientsPage()
        {
            InitializeComponent();

            BindingContext = ServiceLocator.Current.GetInstance<ClientsVueModele>();
        }

        protected override void OnAppearing()
        {
            var vm = (ClientsVueModele)BindingContext;

            vm.ChargerListeCommand.Execute(null);

            base.OnAppearing();
        }

        private async void ClientsListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
                return;

            var Client = new ClientDto { };

            Client = (ClientDto)e.SelectedItem;

            var location = new LocationDto
            {
                ClientLocation = Client.Nom
            };

            LocationDetailPage locationDetailPage = new LocationDetailPage(location);

            //if (locationDetailPage.EstChoisi)
            //{
                await Navigation.PushAsync(new ClientDetailPage((ClientDto)e.SelectedItem));
             
            //}
            //else
            //{
            //    await Navigation.PushAsync(new LocationDetailPage(location));

            //};

            clientsListView.SelectedItem = null;
        }


        private async void Bouton_Supprimer_Clic(object sender, EventArgs e)
        {

            await DisplayAlert("suppression", "Le client a été supprimé avec succès!", "ok");
            await Navigation.PopAsync();
        }

    }
}