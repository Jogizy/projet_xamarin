﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using AutoMapper;
using ProjetComplet.Dtos;
using ProjetComplet.Repositories;
using Xamarin.Forms;

namespace ProjetComplet.VuesModeles
{
    public class ClientDetailVueModele : BaseVueModele
    {
        private int _id;
        private string _nom;
        private string _prenom;
        private string _telephone;
        private string _courriel;
        private string _nip;
        private readonly IMapper _mapper;
        private readonly IClientRepo _clientRepo;

        public int Id { get => _id; set => SetValue(ref _id, value); }
        public string Nom { get => _nom; set => SetValue(ref _nom, value); }
        public string Prenom { get => _prenom; set => SetValue(ref _prenom, value); }
        public string Telephone { get => _telephone; set => SetValue(ref _telephone, value); }
        public string Courriel { get => _courriel; set => SetValue(ref _courriel, value); }
        public string NIP { get => _nip; set => SetValue(ref _nip, value); }



        public ClientDto Client{ get; set; }

        public ICommand SauvegarderCommand { get; set; }

        public ClientDetailVueModele(IMapper mapper, IClientRepo clientRepo)
        {
            _mapper = mapper;
            _clientRepo = clientRepo;
            SauvegarderCommand = new Command(Sauvegarder, PeutSauvegarder);
        }

        // Retourne l'information pour vérifier si il peut sauvegarder
        private bool PeutSauvegarder()
        {
            return !string.IsNullOrWhiteSpace(Nom);
        }


        /// Sauvegarde dans la base de données

        private async void Sauvegarder()
        {
            _mapper.Map(this, Client);

            var clientBd = await _clientRepo.Consulter(Id);
            _mapper.Map(Client, clientBd);

            await _clientRepo.Modifier(clientBd);
        }
    }

}