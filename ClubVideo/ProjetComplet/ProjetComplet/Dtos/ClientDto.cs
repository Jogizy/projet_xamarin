﻿using ProjetComplet.Modeles;
using ProjetComplet.VuesModeles;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetComplet.Dtos
{

    /// <summary>
    ///     Classe qui définit le transfère de données d'un objet Client
    /// </summary>

    public class ClientDto : BaseVueModele
    {
        private int _id;
        private string _nom;
        private string _prenom;
        private string _telephone;
        private string _courriel;
        private string _nip;
        private List<Location> _clientLocations;

        public int Id { get => _id; set => SetValue(ref _id, value); }
        public string Nom { get => _nom; set => SetValue(ref _nom, value); }
        public string Prenom { get => _prenom; set => SetValue(ref _prenom, value); }
        public string Telephone { get => _telephone; set => SetValue(ref _telephone, value); }
        public string Courriel{ get => _courriel; set => SetValue(ref _courriel, value); }
        public string NIP { get => _nip; set => SetValue(ref _nip, value); }
        public List<Location> ClientLocations { get => _clientLocations; set => SetValue(ref _clientLocations, value); }
    }
}