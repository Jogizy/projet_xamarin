﻿using CommonServiceLocator;
using ProjetComplet.Modeles;
using ProjetComplet.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjetComplet.Vues
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Menu : ContentPage
    {
        public Menu()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            var menus = new List<MenuItem>();
            menus.Add(new MenuItem
            {
                Titre = "Gestion des articles",
                Icone = "articles.png",
                Cible = typeof(ArticlesPage)
            });

            if ((await ServiceLocator.Current.GetInstance<IAuthService>().GetRoleUtilisateurConnecte()).Equals(Role.Admin))
                menus.Add(new MenuItem
                {
                    Titre = "Gestion des utilisateurs",
                    Icone = "utilisateur.png",
                    Cible = typeof(UtilisateursPage)
                });


            menus.Add(new MenuItem
            {
                Titre = "Déconnexion",
                Icone = "deconnexion.png",
                Cible = typeof(ConnexionPage)
            });

            MenuListView.ItemsSource = menus;

            base.OnAppearing();
        }
    }
}