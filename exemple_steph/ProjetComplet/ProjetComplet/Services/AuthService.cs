﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using ProjetComplet.Modeles;
using ProjetComplet.Repositories;
using Xamarin.Essentials;

namespace ProjetComplet.Services
{
    public class AuthService : IAuthService
    {
        private const string CleIdUtilisateurConnecte = "UtilisateurConnecte";

        private readonly IUtilisateurRepo _utilisateurRepo;

        private Utilisateur _utilisateurConnecte;
        private int _idUtilisateurConnecte = -2;

        public AuthService(IUtilisateurRepo utilisateurRepo)
        {
            _utilisateurRepo = utilisateurRepo;
        }  

        public async Task<bool> Connexion(string nomUtilisateur, string motDePasse)
        {
            if (string.IsNullOrWhiteSpace(nomUtilisateur) || string.IsNullOrWhiteSpace(motDePasse))
                return false;

            if ((await _utilisateurRepo.Lister()).Count == 0)
                await _utilisateurRepo.AjouterAdmin();

            var utilisateur = await _utilisateurRepo.Consulter(nomUtilisateur);

            if (utilisateur == null)
                return false;

            if (!VerifMotDePasse(utilisateur, motDePasse))
                return false;

            await SecureStorage.SetAsync(CleIdUtilisateurConnecte, utilisateur.Id.ToString());
            _utilisateurConnecte = utilisateur;
            _idUtilisateurConnecte = utilisateur.Id;

            return true;
        }

        private bool VerifMotDePasse(Utilisateur utilisateur, string motDePasse)
        {
            using (var hmac = new HMACSHA512(Convert.FromBase64String(utilisateur.MotDePasseSalt)))
            {
                var hashCalcule = Convert.ToBase64String(hmac.ComputeHash(Encoding.UTF8.GetBytes(motDePasse)));
                return hashCalcule.Equals(utilisateur.MotDePasseHash);
            }
        }

        public void Deconnexion()
        {
            SecureStorage.Remove(CleIdUtilisateurConnecte);
            _utilisateurConnecte = null;
            _idUtilisateurConnecte = -1;
        }

        public async Task<bool> EstConnecte()
        {
            return (await GetIdUtilisateurConnecte()) >= 0;
        }

        public async Task<int> GetIdUtilisateurConnecte()
        {
            if (_idUtilisateurConnecte == -2)
            {
                var id = await SecureStorage.GetAsync(CleIdUtilisateurConnecte);
                _idUtilisateurConnecte = string.IsNullOrWhiteSpace(id) ? -1 : int.Parse(id);
            }

            return _idUtilisateurConnecte;
        }

        public async Task<string> GetNomUtilisateurConnecte()
        {
            return (await GetUtilisateurConnecte())?.NomUtilisateur;
        }

        public async Task<string> GetRoleUtilisateurConnecte()
        {
            return (await GetUtilisateurConnecte())?.Role;
        }

        public async Task<Utilisateur> GetUtilisateurConnecte()
        {
            if (_utilisateurConnecte == null)
            {
                var id = await GetIdUtilisateurConnecte();

                if (id == -1)
                    return null;

                _utilisateurConnecte = await _utilisateurRepo.Consulter(id);
            }

            return _utilisateurConnecte;
        }
    }
}
