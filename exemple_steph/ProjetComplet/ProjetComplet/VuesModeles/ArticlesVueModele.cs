﻿using AutoMapper;
using ProjetComplet.Dtos;
using ProjetComplet.Modeles;
using ProjetComplet.Repositories;
using ProjetComplet.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace ProjetComplet.VuesModeles
{
    public class ArticlesVueModele : BaseVueModele
    {
        private List<ArticleDto> _articles;
        private ArticleDto _articleSelectionne;
        private string _nomArticleRecherche;
        private bool _seulementEstPrefere;
        private readonly IArticleRepo _articleRepo;
        private readonly IMapper _mapper;
        private readonly IAuthService _authService;

        public List<ArticleDto> Articles
        {
            get => _articles?.Where(Filtrer).OrderBy(a => a.Nom).ToList();
        }

        public ArticleDto ArticleSelectionne { get => _articleSelectionne; set => SetValue(ref _articleSelectionne, value); }

        public string NomArticleRecherche {
            get => _nomArticleRecherche;
            set
            {
                _nomArticleRecherche = value;
                OnPropertyChanged(nameof(Articles));
            }
        }

        public bool SeulementEstPrefere
        {
            get => _seulementEstPrefere;
            set
            {
                _seulementEstPrefere = value;
                OnPropertyChanged(nameof(Articles));
            }
        }

        public ICommand ChargerListeCommand { get; set; }
        public ICommand AjouterCommand { get; set; }
        public ICommand SupprimerCommand { get; set; }


        public ArticlesVueModele(IArticleRepo articleRepo, IMapper mapper, IAuthService authService)
        {
            _articleRepo = articleRepo;
            _mapper = mapper;
            _authService = authService;

            ChargerListeCommand = new Command(ChargerListe);
            AjouterCommand = new Command(Ajouter, PeutAjouter);
            SupprimerCommand = new Command<ArticleDto>(Supprimer, PeutSupprimer);
        }

        private bool PeutSupprimer(ArticleDto arg)
        {
            return _authService.GetRoleUtilisateurConnecte().Result.Equals(Role.Admin);
        }

        private async void Supprimer(ArticleDto article)
        {
            var articleBd = await _articleRepo.Consulter(article.Id);
            await _articleRepo.Supprimer(articleBd);
            _articles.Remove(article);
            OnPropertyChanged(nameof(Articles));
        }

        private bool PeutAjouter()
        {
            return _authService.GetRoleUtilisateurConnecte().Result.Equals(Role.Admin);
        }

        private async void Ajouter()
        {
            var article = new Article
            {
                Nom = $"Nouvel article {DateTime.Now}"
            };
            await _articleRepo.Ajouter(article);
            var articleDto = _mapper.Map<ArticleDto>(article);
            _articles.Add(articleDto);
            OnPropertyChanged(nameof(Articles));
            ArticleSelectionne = articleDto;
        }

        private async void ChargerListe()
        {
            if (_articles == null)
                _articles = _mapper.Map<List<ArticleDto>>(await _articleRepo.Lister());
            OnPropertyChanged(nameof(Articles));
        }

        private bool Filtrer(ArticleDto article)
        {
            if (SeulementEstPrefere && !article.EstPrefere)
                return false;

            if (!string.IsNullOrWhiteSpace(NomArticleRecherche))
            {
                var elementsRecherches = NomArticleRecherche.ToLower().Split(' ');
                foreach (var element in elementsRecherches)
                    if (!article.Nom.ToLower().Contains(element))
                        return false;
            }

            return true;
        }
    }
}
