﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ProjetComplet.Persistence
{
    public interface IPersistence
    {
        Task Init();

        Task<T> Ajouter<T>(T item) where T : class;

        Task<T> Consulter<T>(int id) where T : class;
        Task<T> Consulter<T>(Expression<Func<T, bool>> filtre) where T : class;

        Task<List<T>> Lister<T>() where T : class;
        DbSet<T> Ensemble<T>() where T : class;

        T Modifier<T>(T item) where T : class;

        T Supprimer<T>(T item) where T : class;

        Task<int> Sauvegarder();
    }
}
