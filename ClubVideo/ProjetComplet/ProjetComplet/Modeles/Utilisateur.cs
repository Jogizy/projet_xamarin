﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace ProjetComplet.Modeles
{
    /// <summary>
    ///     La classe Utilisateur et ses attributs
    /// </summary>
    public class Utilisateur
    {
        public int Id { get; set; }
        public string NomUtilisateur { get; set; }
        public string MotDePasseHash { get; set; }
        public string MotDePasseSalt { get; set; }
        public string Role { get; set; }

        /// <summary>
        ///     Le Constructeur de  la classe utilisateur
        /// </summary>
        public Utilisateur()
        {
            Role = Modeles.Role.Utilisateur;
        }

        /// <summary>
        ///     Cette méthode définit le mot de passe
        /// </summary>
        /// <param name="motDePasse"> mot de passe de l'utilisateur</param>
        /// <param name="forcer"></param>
        public void SetMotDePasse(string motDePasse, bool forcer = false)
        {
            if (!forcer && string.IsNullOrEmpty(motDePasse))
                return;

            using (var hmac = new HMACSHA512())
            {
                MotDePasseSalt = Convert.ToBase64String(hmac.Key);
                MotDePasseHash = Convert.ToBase64String(hmac.ComputeHash(Encoding.UTF8.GetBytes(motDePasse)));
            }
        }
    }
}
