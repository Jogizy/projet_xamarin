﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace ProjetComplet.Modeles
{
    public class Utilisateur
    {
        public int Id { get; set; }
        public string NomUtilisateur { get; set; }
        public string MotDePasseHash { get; set; }
        public string MotDePasseSalt { get; set; }
        public string Role { get; set; }
        public List<ArticlePrefere> ArticlesPreferes { get; set; }

        public Utilisateur()
        {
            ArticlesPreferes = new List<ArticlePrefere>();
            Role = Modeles.Role.Utilisateur;
        }

        public void SetMotDePasse(string motDePasse, bool forcer = false)
        {
            if (!forcer && string.IsNullOrEmpty(motDePasse))
                return;

            using (var hmac = new HMACSHA512())
            {
                MotDePasseSalt = Convert.ToBase64String(hmac.Key);
                MotDePasseHash = Convert.ToBase64String(hmac.ComputeHash(Encoding.UTF8.GetBytes(motDePasse)));
            }
        }
    }
}
