﻿using ProjetComplet.VuesModeles;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetComplet.Dtos
{

    /// Classe qui définit le transfère de données d'un objet de type Film


    public class FilmDto : BaseVueModele
    {
        private int _id;
        private string _nom;
        private string _categorie;
        private TimeSpan _duree;
        private DateTime _datedesortie;
        private string _description;
        private string _photo;
        private string _bandeannonce;
        private int _quantitetotale;
        private int _quantitedisponible;
        private decimal _prixlocation;

        public int Id { get => _id; set => SetValue(ref _id, value); }
        public string Nom { get => _nom; set => SetValue(ref _nom, value); }
        public string Categorie { get => _categorie; set => SetValue(ref _categorie, value); }
        public TimeSpan Duree { get => _duree; set => SetValue(ref _duree, value); }
        public DateTime DateDeSortie { get => _datedesortie; set => SetValue(ref _datedesortie, value); }
        public string Description { get => _description; set => SetValue(ref _description, value); }
        public string Photo { get => _photo; set => SetValue(ref _photo, value); }
        public string BandeAnnonce { get => _bandeannonce; set => SetValue(ref _bandeannonce, value); }
        public int QuantiteTotale{ get => _quantitetotale; set => SetValue(ref _quantitetotale, value); }
        public int QuantiteDisponible { get => _quantitedisponible; set => SetValue(ref _quantitedisponible, value); }
        public decimal PrixLocation { get => _prixlocation; set => SetValue(ref _prixlocation, value); }
    }
}