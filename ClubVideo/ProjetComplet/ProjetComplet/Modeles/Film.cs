﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace ProjetComplet.Modeles
{

    /// <summary>
    ///     La classe Film et ses attributs
    /// </summary>

    public class Film
    { 
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Categorie { get; set; }
        public TimeSpan Duree { get; set; }
        public DateTime DateDeSortie { get; set; }
        public string Description { get; set; }
        public string Photo { get; set; }
        public string BandeAnnonce { get; set; }
        public int QuantiteTotale { get; set; }
        public int QuantiteDisponible { get; set; }
        public decimal PrixLocation { get; set; }
        public List<Location> Locations { get; set; }

    }
}