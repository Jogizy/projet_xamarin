﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ProjetComplet.Services
{
    public class PageService : IPageService
    {
        // Gère l'affichage de message d'erreur. Le message d'erreur ne doit pas être vide
        public Task AfficherMessageErreur(string message)
        {
            return Application.Current.MainPage.DisplayAlert("Erreur", message, "OK");
        }

        // Gère le chargement de la page principale
        public void ChangerPagePrincipale(Page page)
        {
            Application.Current.MainPage = page;
        }
    }
}
