﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjetComplet.Modeles;
using ProjetComplet.Persistence;
using ProjetComplet.Services;

namespace ProjetComplet.Repositories
{

    /// <summary>
    ///     Classe qui permet de gérer les différentes sources pour le CRUD des films
    /// </summary>

    public class FilmRepo : IFilmRepo
    {
        private readonly IPersistence _bd;
        private readonly IAuthService _authService;

        public FilmRepo(IPersistence bd, IAuthService authService)
        {
            _bd = bd;
            _authService = authService;
        }


        /// <summary>
        ///     Tâche permettant l'ajout d'un film
        /// </summary>
        /// <param name="film">Film à ajouter</param>
        /// <returns></returns>


        public async Task<int> Ajouter(Film film)
        {
            await _bd.Ajouter(film);

            return await _bd.Sauvegarder();
        }




        /// <summary>
        ///     Tâche permettant la consultaion d'un film
        /// </summary>
        /// <param name="id"> identifiant du film à consuter</param>
        /// <returns> Informations sur le film</returns>


        public Task<Film> Consulter(int id)
        {
            return _bd.Ensemble<Film>().SingleOrDefaultAsync(a => a.Id == id);
        }



        /// <summary>
        ///     Tâche permettant d'obtenir la liste des films 
        /// </summary>
        /// <returns>¸Liste de films</returns>

        public Task<List<Film>> Lister()
        {
            return _bd.Ensemble<Film>().ToListAsync();
        }



        /// <summary>
        ///     Tâche permettant la modification du film
        /// </summary>
        /// <param name="film"> client à modifier </param>
        /// <returns>film modifié</returns>

        public async Task<int> Modifier(Film film)
        {
            _bd.Modifier(film);

            return await _bd.Sauvegarder();
        }



        /// <summary>
        ///     Tâche permettant la suppression du film
        /// </summary>
        /// <param name="film"> film à supprimer </param>
        /// <returns>film supprimé de la base de donnée</returns>

        public async Task<int> Supprimer(Film film)
        {
            _bd.Supprimer(film);

            return await _bd.Sauvegarder();
        }
    }
}