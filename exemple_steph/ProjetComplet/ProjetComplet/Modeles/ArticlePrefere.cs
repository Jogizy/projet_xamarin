﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProjetComplet.Modeles
{
    public class ArticlePrefere
    {
        [Required]
        public int UtilisateurId { get; set; }
		[Required]
        public int ArticleId { get; set; }
        public Utilisateur Utilisateur { get; set; }
        public Article Article { get; set; }
    }
}
