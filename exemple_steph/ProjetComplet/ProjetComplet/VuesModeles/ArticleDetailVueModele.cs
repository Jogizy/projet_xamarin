﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using AutoMapper;
using ProjetComplet.Dtos;
using ProjetComplet.Repositories;
using Xamarin.Forms;

namespace ProjetComplet.VuesModeles
{
    public class ArticleDetailVueModele : BaseVueModele
    {
        private int _id;
        private string _nom;
        private int _quantite;
        private decimal _prix;
        private bool _estPrefere;
        private readonly IMapper _mapper;
        private readonly IArticleRepo _articleRepo;

        public int Id { get => _id; set => SetValue(ref _id, value); }
        public string Nom { get => _nom; set => SetValue(ref _nom, value); }
        public int Quantite { get => _quantite; set => SetValue(ref _quantite, value); }
        public decimal Prix { get => _prix; set => SetValue(ref _prix, value); }
        public bool EstPrefere { get => _estPrefere; set => SetValue(ref _estPrefere, value); }

        public ArticleDto Article { get; set; }

        public ICommand SauvegarderCommand { get; set; }

        public ArticleDetailVueModele(IMapper mapper, IArticleRepo articleRepo)
        {
            _mapper = mapper;
            _articleRepo = articleRepo;
            SauvegarderCommand = new Command(Sauvegarder, PeutSauvegarder);
        }

        private bool PeutSauvegarder()
        {
            return !string.IsNullOrWhiteSpace(Nom);
        }

        private async void Sauvegarder()
        {
            _mapper.Map(this, Article);

            var articleBd = await _articleRepo.Consulter(Id);
            _mapper.Map(Article, articleBd);

            await _articleRepo.ModifierArticlePrefere(articleBd, EstPrefere);

            await _articleRepo.Modifier(articleBd);
        }
    }
}
