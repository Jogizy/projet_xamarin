﻿using Microsoft.EntityFrameworkCore;
using ProjetComplet.Modeles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ProjetComplet.Persistence
{
    public class BdContexte : DbContext, IPersistence
    {
        public DbSet<Utilisateur> Utilisateurs { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Film> Films { get; set; }
        public DbSet<Location> Locations { get; set; }


        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task<T> Ajouter<T>(T item) where T : class
        {
            return (await AddAsync(item)).Entity;
        }

        public Task<T> Consulter<T>(int id) where T : class
        {
            return FindAsync<T>(id);
        }

        public Task<T> Consulter<T>(Expression<Func<T, bool>> filtre) where T : class
        {
            return Set<T>().Where(filtre).FirstOrDefaultAsync();
        }

        public DbSet<T> Ensemble<T>() where T : class
        {
            return Set<T>();
        }

        public async Task Init()
        {
            //await Database.EnsureDeletedAsync();
            await Database.MigrateAsync();
        }

        public Task<List<T>> Lister<T>() where T : class
        {
            return Set<T>().ToListAsync();
        }

        public T Modifier<T>(T item) where T : class
        {
            return Update(item).Entity;
        }

        public Task<int> Sauvegarder()
        {
            return SaveChangesAsync();
        }

        public T Supprimer<T>(T item) where T : class
        {
            return Remove(item).Entity;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlite($"Filename={Constantes.CheminBd}");
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Location>()
                .HasOne(l => l.Client)
                .WithMany(c => c.ClientLocations)
                .HasForeignKey(l => l.ClientId);
            builder.Entity<Location>()
                .HasOne(l => l.Film)
                .WithMany(f => f.Locations)
                .HasForeignKey(l => l.FilmId);
        }
    }
}
