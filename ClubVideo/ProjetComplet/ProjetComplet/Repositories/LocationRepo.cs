﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjetComplet.Modeles;
using ProjetComplet.Persistence;
using ProjetComplet.Services;

namespace ProjetComplet.Repositories
{
    public class LocationRepo : ILocationRepo
    {
        private readonly IPersistence _bd;
        private readonly IAuthService _authService;

        public LocationRepo(IPersistence bd, IAuthService authService)
        {
            _bd = bd;
            _authService = authService;
        }


        // Gère l'ajout d'une location dans le base de données

        public async Task<int> Ajouter(Location location)
        {
            await _bd.Ajouter(location);

            return await _bd.Sauvegarder();
        }


        /// Gère la consultation des locations avec un Id

        public Task<Location> Consulter(int id)
        {
            return _bd.Ensemble<Location>().SingleOrDefaultAsync(a => a.Id == id);
        }


        /// Liste les locations de la base de données

        public Task<List<Location>> Lister()
        {
            return _bd.Ensemble<Location>().ToListAsync();
        }


        /// Gère la modification d'une location dans la base de données

        public async Task<int> Modifier(Location location)
        {
            _bd.Modifier(location);

            return await _bd.Sauvegarder();
        }


        /// Gère la suppression d'une location dans la base de données

        public async Task<int> Supprimer(Location location)
        {
            _bd.Supprimer(location);

            return await _bd.Sauvegarder();
        }
    }
}