﻿using AutoMapper;
using ProjetComplet.Dtos;
using ProjetComplet.Modeles;
using ProjetComplet.Repositories;
using ProjetComplet.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace ProjetComplet.VuesModeles
{
    public class UtilisateursVueModele : BaseVueModele
    {
        private List<UtilisateurDto> _utilisateurs;
        private UtilisateurDto utilisateurSelectionne;
        private string _nomUtilisateurRecherche;
        private readonly IUtilisateurRepo _utilisateurRepo;
        private readonly IMapper _mapper;
        private readonly IPageService _pageService;

        public List<UtilisateurDto> Utilisateurs => _utilisateurs?.Where(Filtrer).OrderBy(u => u.NomUtilisateur).ToList();

        public UtilisateurDto UtilisateurSelectionne { get => utilisateurSelectionne; set => SetValue(ref utilisateurSelectionne, value); }


        public string NomUtilisateurRecherche
        {
            get => _nomUtilisateurRecherche;
            set
            {
                _nomUtilisateurRecherche = value;
                OnPropertyChanged(nameof(Utilisateurs));
            }
        }


        public ICommand ChargerListeCommand { get; set; }
        public ICommand AjouterCommand { get; set; }
        public ICommand SupprimerCommand { get; set; }

        public UtilisateursVueModele(IUtilisateurRepo utilisateurRepo, IMapper mapper, IPageService pageService)
        {
            ChargerListeCommand = new Command(ChargerListe);
            AjouterCommand = new Command(Ajouter);
            SupprimerCommand = new Command<UtilisateurDto>(Supprimer);
            _utilisateurRepo = utilisateurRepo;
            _mapper = mapper;
            _pageService = pageService;
        }

        // Supprime l'utilisateur dans la base de données
        private async void Supprimer(UtilisateurDto utilisateur)
        {
            var utilisateurBd = await _utilisateurRepo.Consulter(utilisateur.Id);
            if (await _utilisateurRepo.Supprimer(utilisateurBd) == 0)
                await _pageService.AfficherMessageErreur("Impossible de supprimer cet utilisateur");
            else
            {
                _utilisateurs.Remove(utilisateur);
                OnPropertyChanged(nameof(Utilisateurs));
            }
        }

        // Ajoute un utilisateur dans la base de données
        private async void Ajouter()
        {
            var utilisateur = new Utilisateur
            {
                NomUtilisateur = $"Nouvel utilisateur {DateTime.Now}",
                Role = Role.Utilisateur
            };
            await _utilisateurRepo.Ajouter(utilisateur, "");

            var utilisateurDto = _mapper.Map<UtilisateurDto>(utilisateur);

            _utilisateurs.Add(utilisateurDto);
            OnPropertyChanged(nameof(Utilisateurs));

            UtilisateurSelectionne = utilisateurDto;
        }

        // Charge la liste des utilisateurs
        private async void ChargerListe()
        {
            if (_utilisateurs == null)
                _utilisateurs = _mapper.Map<List<UtilisateurDto>>(await _utilisateurRepo.Lister());
            OnPropertyChanged(nameof(Utilisateurs));
        }


        // Applique le filtre de recherche des utilisateurs
        private bool Filtrer(UtilisateurDto utilisateur)
        {

            if (!string.IsNullOrWhiteSpace(NomUtilisateurRecherche))
            {
                var elementsRecherches = NomUtilisateurRecherche.ToLower().Split(' ');
                foreach (var element in elementsRecherches)
                    if (!utilisateur.NomUtilisateur.ToLower().Contains(element))
                        return false;
            }

            return true;
        }

    }
}
