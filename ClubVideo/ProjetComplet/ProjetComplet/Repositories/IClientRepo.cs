﻿using ProjetComplet.Modeles;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjetComplet.Repositories
{

    /// <summary>
    ///     Interface 
    /// </summary>

    public interface IClientRepo 
    {
        Task<int> Ajouter(Client client);
        Task<Client> Consulter(int id);
        Task<List<Client>>Lister();
        Task<int> Modifier(Client client);
        Task<int> Supprimer(Client client);
    }
}