﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using ProjetComplet.Modeles;
using ProjetComplet.Repositories;
using Xamarin.Essentials;

namespace ProjetComplet.Services
{
    /// <summary>
    ///     Classe qui définit les services liés à l'authentification de l'utilisateur
    /// </summary>
    public class AuthService : IAuthService
    {
        private const string CleIdUtilisateurConnecte = "UtilisateurConnecte";

        private readonly IUtilisateurRepo _utilisateurRepo;

        private Utilisateur _utilisateurConnecte;
        private int _idUtilisateurConnecte = -2;
        /// <summary>
        ///     Constructeur de la classe
        /// </summary>
        /// <param name="utilisateurRepo"></param>
        public AuthService(IUtilisateurRepo utilisateurRepo)
        {
            _utilisateurRepo = utilisateurRepo;
        }  

        // Gère la connexion d'un utilisateur. S'assure que les champs ne sont pas vide, ajoute un Administrateur
        // s'il détecte qu'il n'en a pas et fait la connexion qui persiste jusqu'à ce que l'utilisateur se déconnecte
        public async Task<bool> Connexion(string nomUtilisateur, string motDePasse)
        {
            if (string.IsNullOrWhiteSpace(nomUtilisateur) || string.IsNullOrWhiteSpace(motDePasse))
                return false;

            if ((await _utilisateurRepo.Lister()).Count == 0)
                await _utilisateurRepo.AjouterAdmin();

            var utilisateur = await _utilisateurRepo.Consulter(nomUtilisateur);

            if (utilisateur == null)
                return false;

            if (!VerifMotDePasse(utilisateur, motDePasse))
                return false;

            await SecureStorage.SetAsync(CleIdUtilisateurConnecte, utilisateur.Id.ToString());
            _utilisateurConnecte = utilisateur;
            _idUtilisateurConnecte = utilisateur.Id;

            return true;
        }

        // Vérifie le mot de passe de l'utilisateur
        private bool VerifMotDePasse(Utilisateur utilisateur, string motDePasse)
        {
            using (var hmac = new HMACSHA512(Convert.FromBase64String(utilisateur.MotDePasseSalt)))
            {
                var hashCalcule = Convert.ToBase64String(hmac.ComputeHash(Encoding.UTF8.GetBytes(motDePasse)));
                return hashCalcule.Equals(utilisateur.MotDePasseHash);
            }
        }

        // Gère la déconnexion d'un utilisateur
        public void Deconnexion()
        {
            SecureStorage.Remove(CleIdUtilisateurConnecte);
            _utilisateurConnecte = null;
            _idUtilisateurConnecte = -1;
        }

        // Vérifie si un utilisateur est connecté
        public async Task<bool> EstConnecte()
        {
            return (await GetIdUtilisateurConnecte()) >= 0;
        }

        // Retourne l'ID de l'utilisateur déja connecté
        public async Task<int> GetIdUtilisateurConnecte()
        {
            if (_idUtilisateurConnecte == -2)
            {
                var id = await SecureStorage.GetAsync(CleIdUtilisateurConnecte);
                _idUtilisateurConnecte = string.IsNullOrWhiteSpace(id) ? -1 : int.Parse(id);
            }

            return _idUtilisateurConnecte;
        }

        // Retourne l'utilisateur connecté
        public async Task<string> GetNomUtilisateurConnecte()
        {
            return (await GetUtilisateurConnecte())?.NomUtilisateur;
        }

        // Retourne le rôle de l'utilisateur (Admin ou Utilisateur)
        public async Task<string> GetRoleUtilisateurConnecte()
        {
            return (await GetUtilisateurConnecte())?.Role;
        }

        // Gère la vérification d'un utilisateur déjà connecté
        public async Task<Utilisateur> GetUtilisateurConnecte()
        {
            if (_utilisateurConnecte == null)
            {
                var id = await GetIdUtilisateurConnecte();

                if (id == -1)
                    return null;

                _utilisateurConnecte = await _utilisateurRepo.Consulter(id);
            }

            return _utilisateurConnecte;
        }
    }
}
