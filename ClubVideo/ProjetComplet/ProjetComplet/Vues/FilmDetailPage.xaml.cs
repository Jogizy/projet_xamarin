﻿using AutoMapper;
using CommonServiceLocator;
using ProjetComplet.Dtos;
using ProjetComplet.Services;
using ProjetComplet.VuesModeles;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjetComplet.Vues
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FilmDetailPage : ContentPage
    {
        public FilmDetailPage(FilmDto film)
        {
            InitializeComponent();

            BindingContext = ServiceLocator.Current.GetInstance<IMapper>().Map(film,
                ServiceLocator.Current.GetInstance<FilmDetailVueModele>());
        }


        /// Gère la sauvegarde d'un film

        private void ToolbarItem_Activated(object sender, System.EventArgs e)
        {
            var vm = (FilmDetailVueModele)BindingContext;

            if (vm.SauvegarderCommand.CanExecute(null))
            {
                vm.SauvegarderCommand.Execute(null);

                Navigation.PopAsync();
            }
            else
                ServiceLocator.Current.GetInstance<IPageService>().AfficherMessageErreur("Le nom du film ne peut pas être vide");
        }
    }
}