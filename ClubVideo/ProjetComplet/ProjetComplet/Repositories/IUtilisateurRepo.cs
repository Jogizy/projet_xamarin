﻿using ProjetComplet.Modeles;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjetComplet.Repositories
{
    public interface IUtilisateurRepo
    {
        Task<int> Ajouter(Utilisateur utilisateur, string motDePasse);
        Task<int> AjouterAdmin();
        Task<Utilisateur> Consulter(int id);
        Task<Utilisateur> Consulter(string nomUtilisateur);
        Task<List<Utilisateur>> Lister();
        Task<int> Modifier(Utilisateur utilisateur);
        Task<int> Supprimer(Utilisateur utilisateur);
    }
}
