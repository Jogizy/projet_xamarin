﻿using AutoMapper;
using ProjetComplet.Modeles;
using ProjetComplet.Services;
using ProjetComplet.VuesModeles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjetComplet.Dtos
{

    /// <summary>
    ///     Classe qui définit d'un profil pour transférer un objet dans un autre.
    /// </summary>    
    public class AutoMapperProfils : Profile
    {


        /// <summary>
        /// 
        /// </summary>
        /// <param name="authService">Permet d'obtenir l'utilisateur présentement connecté</param>
        public AutoMapperProfils(IAuthService authService)
        {
           /// Création des mappages de chaque objet
           
            CreateMap<Utilisateur, UtilisateurDto>();
            CreateMap<UtilisateurDto, Utilisateur>()
                .AfterMap((src, dest) => dest.SetMotDePasse(src.MotDePasse));
            CreateMap<UtilisateurDto, UtilisateurDetailVueModele>()
                .AfterMap((src, dest) => dest.Utilisateur = src);
            CreateMap<UtilisateurDetailVueModele, UtilisateurDto>();

            CreateMap<ClientDto, Client>();
            CreateMap<Client, ClientDto>();
            CreateMap<ClientDto, ClientDetailVueModele>()
                .AfterMap((src, dest) => dest.Client = src);
            CreateMap<ClientDetailVueModele, ClientDto>();

            CreateMap<FilmDto, Film>();
            CreateMap<Film, FilmDto>();
            CreateMap<FilmDto, FilmDetailVueModele>()
            .AfterMap((src, dest) => dest.Film = src);
            CreateMap<FilmDetailVueModele, FilmDto>();


            CreateMap<LocationDto, Location>();
            CreateMap<Location, LocationDto>();
            CreateMap<LocationDto, LocationDetailVueModele>()
            .AfterMap((src, dest) => dest.Location = src);
            CreateMap<LocationDetailVueModele, LocationDto>();
        }
    }
}
