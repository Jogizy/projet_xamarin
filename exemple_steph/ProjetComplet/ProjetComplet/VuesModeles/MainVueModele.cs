﻿using ProjetComplet.Services;
using ProjetComplet.Vues;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace ProjetComplet.VuesModeles
{
    public class MainVueModele : BaseVueModele
    {
        private readonly IAuthService _authService;
        private readonly IPageService _pageService;

        public ICommand DeconnexionCommand { get; set; }

        public MainVueModele(IAuthService authService, IPageService pageService)
        {
            DeconnexionCommand = new Command(Deconnexion);
            _authService = authService;
            _pageService = pageService;
        }

        private void Deconnexion(object obj)
        {
            _authService.Deconnexion();
            _pageService.ChangerPagePrincipale(new ConnexionPage());
        }
    }
}
