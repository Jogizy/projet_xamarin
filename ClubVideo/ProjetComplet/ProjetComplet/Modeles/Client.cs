﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace ProjetComplet.Modeles
{
    /// La classe Client et ses attributs

    public class Client 
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Prenom{ get; set; }
        public string Telephone { get; set; }
        public string Courriel { get; set; }
        public string NIP { get; set; }



        /// La liste des locations du client
        public List<Location> ClientLocations { get; set; }

        public Client()
        {
           ClientLocations = new List<Location>();
        }
    }
}