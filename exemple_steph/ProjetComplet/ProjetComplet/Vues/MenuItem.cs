﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetComplet.Vues
{
    public class MenuItem
    {
        public string Titre { get; set; }
        public string Icone { get; set; }
        public Type Cible { get; set; }
    }
}
