﻿using ProjetComplet.Modeles;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjetComplet.Repositories
{
    public interface IFilmRepo
    {
        Task<int> Ajouter(Film film);
        Task<Film> Consulter(int id);
        Task<List<Film>> Lister();
        Task<int> Modifier(Film film);
        Task<int> Supprimer(Film film);
    }
}
