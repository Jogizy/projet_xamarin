﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetComplet.Modeles
{
    public class Article
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public int Quantite { get; set; }
        public decimal Prix { get; set; }
        public List<ArticlePrefere> UtilisateursArticlePrefere { get; set; }

        public Article()
        {
            UtilisateursArticlePrefere = new List<ArticlePrefere>();
        }
    }
}
