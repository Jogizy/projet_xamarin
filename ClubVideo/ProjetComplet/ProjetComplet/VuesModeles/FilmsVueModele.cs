﻿using AutoMapper;
using ProjetComplet.Dtos;
using ProjetComplet.Modeles;
using ProjetComplet.Repositories;
using ProjetComplet.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace ProjetComplet.VuesModeles
{
    public class FilmsVueModele : BaseVueModele
    {
        private List<FilmDto> _films;
        private FilmDto _filmSelectionne;
        private string _nomFilmRecherche;
        private readonly IFilmRepo _filmRepo;
        private readonly IMapper _mapper;
        private readonly IAuthService _authService;

        public List<FilmDto> Films
        {
            get => _films?.Where(Filtrer).OrderBy(a => a.Nom).ToList();
        }

        public FilmDto FilmSelectionne { get => _filmSelectionne; set => SetValue(ref _filmSelectionne, value); }

        public string NomFilmRecherche
        {
            get => _nomFilmRecherche;
            set
            {
                _nomFilmRecherche = value;
                OnPropertyChanged(nameof(Films));
            }
        }


        public ICommand ChargerListeCommand { get; set; }
        public ICommand AjouterCommand { get; set; }
        public ICommand SupprimerCommand { get; set; }


        public FilmsVueModele(IFilmRepo filmRepo, IMapper mapper, IAuthService authService)
        {
            _filmRepo = filmRepo;
            _mapper = mapper;
            _authService = authService;

            ChargerListeCommand = new Command(ChargerListe);
            AjouterCommand = new Command(Ajouter, PeutAjouter);
            SupprimerCommand = new Command<FilmDto>(Supprimer, PeutSupprimer);
        }


        /// Retourne l'information pour vérifier si il peut supprimer
        private bool PeutSupprimer(FilmDto arg)
        {
            return _authService.GetRoleUtilisateurConnecte().Result.Equals(Role.Admin);
        }


        /// Supprime le film dans la base de données

        private async void Supprimer(FilmDto film)
        {
            var filmBd = await _filmRepo.Consulter(film.Id);
            await _filmRepo.Supprimer(filmBd);
            _films.Remove(film);
            OnPropertyChanged(nameof(Films));
        }


        /// Vérifie si l'utilisateur peut ajouter un film
        private bool PeutAjouter()
        {
            return _authService.GetRoleUtilisateurConnecte().Result.Equals(Role.Admin);
        }

        /// Ajoute un film dans la bd
        private async void Ajouter()
        {
            var film = new Film
            {
                Nom = $"Nouveau film {DateTime.Now}",

            };
            await _filmRepo.Ajouter(film);
            var filmDto = _mapper.Map<FilmDto>(film);
            _films.Add(filmDto);
            OnPropertyChanged(nameof(Films));
            FilmSelectionne = filmDto;
        }


        private async void ChargerListe()
        {
            if (_films == null)
                _films = _mapper.Map<List<FilmDto>>(await _filmRepo.Lister());
            OnPropertyChanged(nameof(Films));
        }

        private bool Filtrer(FilmDto film)
        {

            if (!string.IsNullOrWhiteSpace(NomFilmRecherche))
            {
                var elementsRecherches = NomFilmRecherche.ToLower().Split(' ');
                foreach (var element in elementsRecherches)
                    if (!film.Nom.ToLower().Contains(element))
                        return false;
            }

            return true;
        }
    }
}
