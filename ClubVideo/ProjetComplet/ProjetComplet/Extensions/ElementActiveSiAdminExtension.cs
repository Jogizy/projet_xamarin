﻿using CommonServiceLocator;
using ProjetComplet.Modeles;
using ProjetComplet.Services;
using System;
using Xamarin.Forms.Xaml;

namespace ProjetComplet.Extensions
{
    public class ElementActiveSiAdminExtension : IMarkupExtension
    {
        public object ProvideValue(IServiceProvider serviceProvider)
        {
            return ServiceLocator.Current.GetInstance<IAuthService>().GetRoleUtilisateurConnecte().Result.Equals(Role.Admin);
        }
    }
}
