﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ProjetComplet
{
    public static class Constantes
    {
        public static readonly string CheminBd = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "ClubVideo.db3");
        public const string NomUtilisateurNonSupprimable = "admin";
    }
}
