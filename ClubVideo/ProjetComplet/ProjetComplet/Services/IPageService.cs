﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ProjetComplet.Services
{
    public interface IPageService
    {
        void ChangerPagePrincipale(Page page);
        Task AfficherMessageErreur(string message);
    }
}
