﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace ProjetComplet.VuesModeles
{
    public class BaseVueModele : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string nomPropriete = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nomPropriete));
        }

        protected void SetValue<T>(ref T attribut, T valeur, [CallerMemberName] string nomPropriete = null)
        {
            if (EqualityComparer<T>.Default.Equals(attribut, valeur))
                return;

            attribut = valeur;

            OnPropertyChanged(nomPropriete);
        }
    }
}
