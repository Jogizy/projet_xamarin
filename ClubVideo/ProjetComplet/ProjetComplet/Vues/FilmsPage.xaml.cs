﻿using CommonServiceLocator;
using ProjetComplet.Dtos;
using ProjetComplet.VuesModeles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjetComplet.Vues
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FilmsPage : ContentPage
    {
        public FilmsPage()
        {
            InitializeComponent();

            BindingContext = ServiceLocator.Current.GetInstance<FilmsVueModele>();
        }

        protected override void OnAppearing()
        {
            var vm = (FilmsVueModele)BindingContext;

            vm.ChargerListeCommand.Execute(null);

            base.OnAppearing();
        }


        // Gère la sélection d'un film dans la liste de films

        private async void FilmsListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
                return;

            var Film = new FilmDto { };

            Film = (FilmDto)e.SelectedItem;

            var location = new LocationDto 
            {
              FilmLocation = Film.Nom
            };

                await Navigation.PushAsync(new FilmDetailPage((FilmDto)e.SelectedItem));
               

            FilmsListView.SelectedItem = null;
        }

        private async void Bouton_Supprimer_Clic(object sender, EventArgs e)
        {
             
            await DisplayAlert("suppression", "le film a été supprimé avec succès!", "ok");
            await Navigation.PopAsync();
        }

    }
}