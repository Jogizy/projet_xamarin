﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetComplet.Modeles
{
    /// La classe statique Role de l'utilisateur 
    public static class Role
    {
        public const string Admin = "Admin";
        public const string Utilisateur = "Utilisateur";
        public static readonly List<string> Liste = new List<string> { Admin, Utilisateur };
    }
}
