﻿using CommonServiceLocator;
using ProjetComplet.Dtos;
using ProjetComplet.VuesModeles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjetComplet.Vues
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UtilisateursPage : ContentPage
    {
        public UtilisateursPage()
        {
            InitializeComponent();

            BindingContext = ServiceLocator.Current.GetInstance<UtilisateursVueModele>();
        }

        protected override void OnAppearing()
        {
            var vm = (UtilisateursVueModele)BindingContext;

            vm.ChargerListeCommand.Execute(null);

            base.OnAppearing();
        }

        private async void UtilisateursListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
                return;

            await Navigation.PushAsync(new UtilisateurDetailPage((UtilisateurDto)e.SelectedItem));

            utilisateursListView.SelectedItem = null;
        }
    }
}