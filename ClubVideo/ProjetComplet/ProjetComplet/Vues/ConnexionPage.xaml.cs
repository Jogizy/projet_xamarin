﻿using CommonServiceLocator;
using ProjetComplet.VuesModeles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjetComplet.Vues
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConnexionPage : ContentPage
    {
        public ConnexionPage()
        {
            InitializeComponent();

            BindingContext = ServiceLocator.Current.GetInstance<ConnexionVueModele>();
        }

        // Gère le mot de passe
        private void NomUtilisateur_Completed(object sender, EventArgs e)
        {
            MotDePasseEntry.Focus();
        }

        // Gère la connexion en vérifiant les informations
        private void MotDePasse_Completed(object sender, EventArgs e)
        {
            var vm = (ConnexionVueModele)BindingContext;
            if (vm.ConnexionCommand.CanExecute(null))
                vm.ConnexionCommand.Execute(null);
        }
    }
}