﻿using System;
using AutoMapper;
using ProjetComplet.Modeles;
using CommonServiceLocator;
using ProjetComplet.Dtos;
using ProjetComplet.Services;
using ProjetComplet.VuesModeles;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjetComplet.Vues
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LocationDetailPage : ContentPage
    {
        public bool EstChoisi;
         
        public LocationDetailPage(LocationDto location)
        {
            InitializeComponent();

            BindingContext = ServiceLocator.Current.GetInstance<IMapper>().Map(location,
                ServiceLocator.Current.GetInstance<LocationDetailVueModele>());

            EstChoisi = false;

        }


        // Gère l'action de sauvegarde

        private void ToolbarItem_Activated(object sender, System.EventArgs e)
        {
            var vm = (LocationDetailVueModele)BindingContext;

            if (vm.SauvegarderCommand.CanExecute(null))
            {
                vm.SauvegarderCommand.Execute(null);

                Navigation.PopAsync();
            }
            else
                ServiceLocator.Current.GetInstance<IPageService>().AfficherMessageErreur("Le nom du Location ne peut pas être vide");
        }



        // Fait pas grand chose

        private  void Bouton_Choisir_Clic (object sender, EventArgs e)
        {
            //EstChoisi = true;
            //await Navigation.PushAsync(new FilmsPage() { 
            //});
           
        }


        // Choisi le client avec le picker
        private void Bouton_Choisir_Client_Clic(object sender, EventArgs e)

        {
            var b = (ClientsVueModele)BindingContext;
            picker1.ItemsSource = b.Clients;
        }

    }
}