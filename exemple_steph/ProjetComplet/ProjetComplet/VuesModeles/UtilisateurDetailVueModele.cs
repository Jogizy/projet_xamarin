﻿using AutoMapper;
using ProjetComplet.Dtos;
using ProjetComplet.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace ProjetComplet.VuesModeles
{
    public class UtilisateurDetailVueModele : BaseVueModele
    {
        private string _nomUtilisateur;
        private string _motDePasse;
        private string _motDePasseConfirmation;
        private string _role;
        private readonly IUtilisateurRepo _utilisateurRepo;
        private readonly IMapper _mapper;

        public string MessageErreur { get; set; }
        public int Id { get; set; }
        public string NomUtilisateur { get => _nomUtilisateur; set => SetValue(ref _nomUtilisateur, value); }
        public string MotDePasse { get => _motDePasse; set => SetValue(ref _motDePasse, value); }
        public string MotDePasseConfirmation { get => _motDePasseConfirmation; set => SetValue(ref _motDePasseConfirmation, value); }
        public string Role { get => _role; set => SetValue(ref _role, value); }

        public bool NomUtilisateurModifiable => !NomUtilisateur.Equals(Constantes.NomUtilisateurNonSupprimable);
        public bool RoleModifiable => !NomUtilisateur.Equals(Constantes.NomUtilisateurNonSupprimable);

        public UtilisateurDto Utilisateur { get; set; }

        public ICommand SauvegarderCommand { get; set; }

        public UtilisateurDetailVueModele(IUtilisateurRepo utilisateurRepo, IMapper mapper)
        {
            SauvegarderCommand = new Command(Sauvegarder, PeutSauvegarder);
            _utilisateurRepo = utilisateurRepo;
            _mapper = mapper;
        }

        private bool PeutSauvegarder()
        {
            MessageErreur = "";
            if (string.IsNullOrWhiteSpace(NomUtilisateur))
                MessageErreur = "Le nom d'utilsiateur ne peut pas être vide";
            else if (MotDePasse != MotDePasseConfirmation)
                MessageErreur = "La confirmation du mot de passe ne correspond pas au mot de passe";

            var utilisateurAyantCeNom = _utilisateurRepo.Consulter(NomUtilisateur).Result;
            if (utilisateurAyantCeNom != null && utilisateurAyantCeNom.Id != Id)
                MessageErreur = "Ce nom d'utilisateur est déjà utilisé";

            return string.IsNullOrEmpty(MessageErreur);
        }

        private async void Sauvegarder()
        {
            _mapper.Map(this, Utilisateur);

            var utilisateurBd = await _utilisateurRepo.Consulter(Id);
            _mapper.Map(Utilisateur, utilisateurBd);

            await _utilisateurRepo.Modifier(utilisateurBd);
        }
    }
}
