﻿using Autofac;
using Autofac.Extras.CommonServiceLocator;
using AutoMapper;
using CommonServiceLocator;
using ProjetComplet.Persistence;
using ProjetComplet.Repositories;
using ProjetComplet.Services;
using ProjetComplet.VuesModeles;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetComplet
{
    public sealed class Bootstrapper
    {
        public static void Initialize()
        {
            ContainerBuilder builder = new ContainerBuilder();

            AjouterBd(builder);

            AjouterRepos(builder);

            AjouterSerivces(builder);

            AjouterAutoMapper(builder);

            AjouterVuesModeles(builder);

            IContainer container = builder.Build();
            AutofacServiceLocator asl = new AutofacServiceLocator(container);
            ServiceLocator.SetLocatorProvider(() => asl);
        }

        private static void AjouterAutoMapper(ContainerBuilder builder)
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            builder.RegisterAssemblyTypes(assemblies)
                .Where(t => typeof(Profile).IsAssignableFrom(t) && !t.IsAbstract && t.IsPublic)
                .As<Profile>();
            builder.Register(c => new MapperConfiguration(cfg =>
            {
                foreach (var profile in c.Resolve<IEnumerable<Profile>>())
                {
                    cfg.AddProfile(profile);
                }
            })).AsSelf().SingleInstance();
            builder.Register(c => c.Resolve<MapperConfiguration>()
                .CreateMapper(c.Resolve))
                .As<IMapper>()
                .InstancePerLifetimeScope();
        }

        private static void AjouterRepos(ContainerBuilder builder)
        {
            builder.RegisterType<UtilisateurRepo>().As<IUtilisateurRepo>().InstancePerLifetimeScope();
            builder.RegisterType<ArticleRepo>().As<IArticleRepo>().InstancePerLifetimeScope();
        }

        private static void AjouterSerivces(ContainerBuilder builder)
        {
            builder.RegisterType<PageService>().As<IPageService>().InstancePerLifetimeScope();
            builder.RegisterType<AuthService>().As<IAuthService>().InstancePerLifetimeScope();
        }

        private static void AjouterVuesModeles(ContainerBuilder builder)
        {
            builder.RegisterType<ConnexionVueModele>().AsSelf();
            builder.RegisterType<MainVueModele>().AsSelf();
            builder.RegisterType<UtilisateursVueModele>().AsSelf();
            builder.RegisterType<UtilisateurDetailVueModele>().AsSelf();
            builder.RegisterType<ArticlesVueModele>().AsSelf();
            builder.RegisterType<ArticleDetailVueModele>().AsSelf();
        }

        private static void AjouterBd(ContainerBuilder builder)
        {
            builder.RegisterType<BdContexte>().As<IPersistence>().SingleInstance();
        }
    }
}
