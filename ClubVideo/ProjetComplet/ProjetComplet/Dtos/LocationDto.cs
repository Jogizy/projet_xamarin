﻿using ProjetComplet.Modeles;
using ProjetComplet.VuesModeles;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetComplet.Dtos
{
    /// Classe qui permet de faire le transfère de données d'un objet de type Location
   public class LocationDto : BaseVueModele
    {
        private int _id;
        private string _client;
        private string _film;
        private bool _retour;
        private DateTime _datelocation;
        private DateTime _dateretourprevu;
        private DateTime _dateretour;
        private string _idcopie;
       


        public int Id { get => _id; set => SetValue(ref _id, value); }
        public string ClientLocation { get => _client; set => SetValue(ref _client, value); }
        public string FilmLocation { get => _film; set => SetValue(ref _film, value); }
        public bool Retour { get => _retour; set => SetValue(ref _retour, value); }
        public DateTime DateLocation { get => _datelocation; set => SetValue(ref _datelocation, value); }
        public DateTime DateRetourPrevu { get => _dateretourprevu; set => SetValue(ref _dateretourprevu, value); }
        public DateTime DateRetour { get => _dateretour; set => SetValue(ref _dateretour, value); }
        public string IdCopie { get => _idcopie; set => SetValue(ref _idcopie, value); }

     
    }
}