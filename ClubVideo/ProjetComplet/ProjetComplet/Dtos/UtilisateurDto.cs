﻿using ProjetComplet.VuesModeles;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetComplet.Dtos
{
    /// <summary>
    ///     Classe qui définit le transfère de données d'un objet de type Utilisateur
    /// </summary>
    public class UtilisateurDto : BaseVueModele
    {
        private string _nomUtilisateur;
        private string _motDePasse;
        private string _motDePasseConfirmation;
        private string _role;

        public int Id { get; set; }
        public string NomUtilisateur { get => _nomUtilisateur; set => SetValue(ref _nomUtilisateur, value); }
        public string MotDePasse { get => _motDePasse; set => SetValue(ref _motDePasse, value); }
        public string MotDePasseConfirmation { get => _motDePasseConfirmation; set => SetValue(ref _motDePasseConfirmation, value); }
        public string Role { get => _role; set => SetValue(ref _role, value); }
    }
}
