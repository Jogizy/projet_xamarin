﻿using CommonServiceLocator;
using ProjetComplet.Modeles;
using ProjetComplet.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjetComplet.Vues
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Menu : ContentPage
    {
        public Menu()
        {
            InitializeComponent();
        }

        // Attributs du menu
        protected override async void OnAppearing()
        {
            var menus = new List<MenuItem>();

            menus.Add(new MenuItem
            {
                Titre = "Gestion des Locations",
                Icone = "clubvideo.png",
                Cible = typeof(LocationsPage)
            });

            menus.Add(new MenuItem
            {
                Titre = "Gestion des Films",
                Icone = "clubvideo.png",
                Cible = typeof(FilmsPage)
            });


            menus.Add(new MenuItem
            {
                Titre = "Gestion des Clients",
                Icone = "utilisateur.png",
                Cible = typeof(ClientsPage)
            });


            if ((await ServiceLocator.Current.GetInstance<IAuthService>().GetRoleUtilisateurConnecte()).Equals(Role.Admin))
                menus.Add(new MenuItem
                {
                    Titre = "Gestion des Employés",
                    Icone = "utilisateur.png",
                    Cible = typeof(UtilisateursPage)
                });


            menus.Add(new MenuItem
            {
                Titre = "Déconnexion",
                Icone = "deconnexion.png",
                Cible = typeof(ConnexionPage)
            });

            MenuListView.ItemsSource = menus;

            base.OnAppearing();
        }
    }
}