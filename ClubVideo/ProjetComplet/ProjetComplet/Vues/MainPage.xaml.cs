﻿using CommonServiceLocator;
using ProjetComplet.VuesModeles;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ProjetComplet.Vues
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : MasterDetailPage
    {
        public MainPage()
        {
            InitializeComponent();

            BindingContext = ServiceLocator.Current.GetInstance<MainVueModele>();

            menu.MenuListView.ItemSelected += OnItemSelected;

            if (Device.RuntimePlatform == Device.UWP)
                MasterBehavior = MasterBehavior.Popover;
        }

        private void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as MenuItem;
            if (item != null)
            {
                if (item.Cible != typeof(ConnexionPage))
                {
                    Detail = new NavigationPage((Page)Activator.CreateInstance(item.Cible));
                    menu.MenuListView.SelectedItem = null;
                    IsPresented = false;
                }
                else
                {
                    var vm = (MainVueModele)BindingContext;
                    vm.DeconnexionCommand.Execute(null);
                }
            }
        }
    }
}
