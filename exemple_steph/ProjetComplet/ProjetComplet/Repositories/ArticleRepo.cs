﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjetComplet.Modeles;
using ProjetComplet.Persistence;
using ProjetComplet.Services;

namespace ProjetComplet.Repositories
{
    public class ArticleRepo : IArticleRepo
    {
        private readonly IPersistence _bd;
        private readonly IAuthService _authService;

        public ArticleRepo(IPersistence bd, IAuthService authService)
        {
            _bd = bd;
            _authService = authService;
        }

        public async Task<int> Ajouter(Article article)
        {
            await _bd.Ajouter(article);

            return await _bd.Sauvegarder();
        }

        public Task<Article> Consulter(int id)
        {
            return _bd.Ensemble<Article>().Include(a => a.UtilisateursArticlePrefere).SingleOrDefaultAsync(a => a.Id == id);
        }

        public Task<List<Article>> Lister()
        {
            return _bd.Ensemble<Article>().Include(a => a.UtilisateursArticlePrefere).OrderBy(a => a.Nom).ToListAsync();
        }

        public async Task<int> Modifier(Article article)
        {
            _bd.Modifier(article);

            return await _bd.Sauvegarder();
        }

        public async Task ModifierArticlePrefere(Article article, bool estPrefere)
        {
            var utilisateurId = await _authService.GetIdUtilisateurConnecte();
            if (estPrefere)
            {
                if (!article.UtilisateursArticlePrefere.Any(ap => ap.UtilisateurId == utilisateurId))
                    article.UtilisateursArticlePrefere.Add(new ArticlePrefere
                    {
                        ArticleId = article.Id,
                        UtilisateurId = utilisateurId
                    });
            }
            else
                article.UtilisateursArticlePrefere.RemoveAll(ap => ap.UtilisateurId == utilisateurId);

        }

        public async Task<int> Supprimer(Article article)
        {
            _bd.Supprimer(article);

            return await _bd.Sauvegarder();
        }
    }
}
