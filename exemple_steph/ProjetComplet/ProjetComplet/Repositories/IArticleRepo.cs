﻿using ProjetComplet.Modeles;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjetComplet.Repositories
{
    public interface IArticleRepo
    {
        Task<int> Ajouter(Article article);
        Task<Article> Consulter(int id);
        Task<List<Article>> Lister();
        Task<int> Modifier(Article article);
        Task<int> Supprimer(Article article);
        Task ModifierArticlePrefere(Article article, bool estPrefere);
    }
}
