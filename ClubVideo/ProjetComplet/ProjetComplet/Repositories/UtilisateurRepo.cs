﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ProjetComplet.Modeles;
using ProjetComplet.Persistence;

namespace ProjetComplet.Repositories
{
    public class UtilisateurRepo : IUtilisateurRepo
    {
        private readonly IPersistence _bd;

        public UtilisateurRepo(IPersistence bd)
        {
            _bd = bd;
        }

        // Gère l'ajout d'un utilisateur et son mot de passe dans le base de données
        public async Task<int> Ajouter(Utilisateur utilisateur, string motDePasse)
        {
            utilisateur.NomUtilisateur = utilisateur.NomUtilisateur.ToLower();

            if (await Consulter(utilisateur.NomUtilisateur) != null)
                return 0;

            utilisateur.SetMotDePasse(motDePasse, forcer: true);

            await _bd.Ajouter(utilisateur);

            return await _bd.Sauvegarder();
        }

        // Ajoute un Administrateur. Un Administrateur ne peut pas être supprimé
        public Task<int> AjouterAdmin()
        {
            var admin = new Utilisateur
            {
                NomUtilisateur = Constantes.NomUtilisateurNonSupprimable,
                Role = Role.Admin
            };

            return Ajouter(admin, Constantes.NomUtilisateurNonSupprimable);
        }

        // Gère la consultation des utilisateurs par son Id
        public Task<Utilisateur> Consulter(int id)
        {
            return _bd.Consulter<Utilisateur>(id);
        }

        // Gère la consultation des utilisateurs par son nom
        public Task<Utilisateur> Consulter(string nomUtilisateur)
        {
            return _bd.Consulter<Utilisateur>(u => u.NomUtilisateur.ToLower() == nomUtilisateur.ToLower());
        }

        // Liste les utilisateurs de la base de données
        public Task<List<Utilisateur>> Lister()
        {
            return _bd.Lister<Utilisateur>();
        }

        // Gère la modification d'un utilisateur dans la base de données
        public Task<int> Modifier(Utilisateur utilisateur)
        {
            _bd.Modifier(utilisateur);

            return _bd.Sauvegarder();
        }

        // Gère la suppression d'un utilisateur dans la base de données. L'utilisateur ne doit pas être un administrateur
        public async Task<int> Supprimer(Utilisateur utilisateur)
        {
            if (utilisateur.NomUtilisateur == Constantes.NomUtilisateurNonSupprimable)
                return 0;

            _bd.Supprimer(utilisateur);

            return await _bd.Sauvegarder();
        }
    }
}
