﻿using CommonServiceLocator;
using ProjetComplet.Dtos;
using ProjetComplet.VuesModeles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjetComplet.Vues
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ArticlesPage : ContentPage
    {
        public ArticlesPage()
        {
            InitializeComponent();

            BindingContext = ServiceLocator.Current.GetInstance<ArticlesVueModele>();
        }

        protected override void OnAppearing()
        {
            var vm = (ArticlesVueModele)BindingContext;

            vm.ChargerListeCommand.Execute(null);

            base.OnAppearing();
        }

        private async void ArticlesListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
                return;

            await Navigation.PushAsync(new ArticleDetailPage((ArticleDto)e.SelectedItem));

            articlesListView.SelectedItem = null;
        }
    }
}