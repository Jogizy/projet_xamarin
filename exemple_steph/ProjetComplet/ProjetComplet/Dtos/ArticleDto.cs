﻿using ProjetComplet.VuesModeles;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetComplet.Dtos
{
    public class ArticleDto : BaseVueModele
    {
        private int _id;
        private string _nom;
        private int _quantite;
        private decimal _prix;
        private bool _estPrefere;

        public int Id { get => _id; set => SetValue(ref _id, value); }
        public string Nom { get => _nom; set => SetValue(ref _nom, value); }
        public int Quantite { get => _quantite; set => SetValue(ref _quantite, value); }
        public decimal Prix { get => _prix; set => SetValue(ref _prix, value); }
        public bool EstPrefere { get => _estPrefere; set => SetValue(ref _estPrefere, value); }
    }
}
