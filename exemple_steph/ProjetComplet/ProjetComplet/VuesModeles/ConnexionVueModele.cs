﻿using ProjetComplet.Services;
using ProjetComplet.Vues;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace ProjetComplet.VuesModeles
{
    public class ConnexionVueModele : BaseVueModele
    {
        private readonly IPageService _pageService;
        private readonly IAuthService _authService;

        public string NomUtilisateur { get; set; }
        public string MotDePasse { get; set; }
        public ICommand ConnexionCommand { get; set; }

        public ConnexionVueModele(IPageService pageService, IAuthService authService)
        {
            ConnexionCommand = new Command(Connexion);
            _pageService = pageService;
            _authService = authService;
        }

        private async void Connexion()
        {
            if (await _authService.Connexion(NomUtilisateur, MotDePasse))
                _pageService.ChangerPagePrincipale(new MainPage());
            else
                await _pageService.AfficherMessageErreur("Nom d'utilisateur ou mot de passe incorrect");
        }
    }
}
