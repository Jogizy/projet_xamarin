﻿using AutoMapper;
using CommonServiceLocator;
using ProjetComplet.Dtos;
using ProjetComplet.Services;
using ProjetComplet.VuesModeles;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjetComplet.Vues
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ClientDetailPage : ContentPage
    {
        public ClientDetailPage(ClientDto client)
        {
            InitializeComponent();

            BindingContext = ServiceLocator.Current.GetInstance<IMapper>().Map(client,
                ServiceLocator.Current.GetInstance<ClientDetailVueModele>());
        }

        private void ToolbarItem_Activated(object sender, System.EventArgs e)
        {
            var vm = (ClientDetailVueModele)BindingContext;

            if (vm.SauvegarderCommand.CanExecute(null))
            {
                vm.SauvegarderCommand.Execute(null);

                Navigation.PopAsync();
            }
            else
                ServiceLocator.Current.GetInstance<IPageService>().AfficherMessageErreur("Le nom du client ne peut pas être vide");
        }
    }
   
}