﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ProjetComplet.Services
{
    public class PageService : IPageService
    {
        public Task AfficherMessageErreur(string message)
        {
            return Application.Current.MainPage.DisplayAlert("Erreur", message, "OK");
        }

        public void ChangerPagePrincipale(Page page)
        {
            Application.Current.MainPage = page;
        }
    }
}
