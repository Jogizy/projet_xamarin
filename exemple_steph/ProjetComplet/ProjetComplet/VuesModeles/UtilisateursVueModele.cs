﻿using AutoMapper;
using ProjetComplet.Dtos;
using ProjetComplet.Modeles;
using ProjetComplet.Repositories;
using ProjetComplet.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace ProjetComplet.VuesModeles
{
    public class UtilisateursVueModele : BaseVueModele
    {
        private List<UtilisateurDto> _utilisateurs;
        private UtilisateurDto utilisateurSelectionne;
        private readonly IUtilisateurRepo _utilisateurRepo;
        private readonly IMapper _mapper;
        private readonly IPageService _pageService;

        public List<UtilisateurDto> Utilisateurs => _utilisateurs?.OrderBy(u => u.NomUtilisateur).ToList();

        public UtilisateurDto UtilisateurSelectionne { get => utilisateurSelectionne; set => SetValue(ref utilisateurSelectionne, value); }

        public ICommand ChargerListeCommand { get; set; }
        public ICommand AjouterCommand { get; set; }
        public ICommand SupprimerCommand { get; set; }

        public UtilisateursVueModele(IUtilisateurRepo utilisateurRepo, IMapper mapper, IPageService pageService)
        {
            ChargerListeCommand = new Command(ChargerListe);
            AjouterCommand = new Command(Ajouter);
            SupprimerCommand = new Command<UtilisateurDto>(Supprimer);
            _utilisateurRepo = utilisateurRepo;
            _mapper = mapper;
            _pageService = pageService;
        }

        private async void Supprimer(UtilisateurDto utilisateur)
        {
            var utilisateurBd = await _utilisateurRepo.Consulter(utilisateur.Id);
            if (await _utilisateurRepo.Supprimer(utilisateurBd) == 0)
                await _pageService.AfficherMessageErreur("Impossible de supprimer cet utilisateur");
            else
            {
                _utilisateurs.Remove(utilisateur);
                OnPropertyChanged(nameof(Utilisateurs));
            }
        }

        private async void Ajouter()
        {
            var utilisateur = new Utilisateur
            {
                NomUtilisateur = $"Nouvel utilisateur {DateTime.Now}",
                Role = Role.Utilisateur
            };
            await _utilisateurRepo.Ajouter(utilisateur, "");

            var utilisateurDto = _mapper.Map<UtilisateurDto>(utilisateur);

            _utilisateurs.Add(utilisateurDto);
            OnPropertyChanged(nameof(Utilisateurs));

            UtilisateurSelectionne = utilisateurDto;
        }

        private async void ChargerListe()
        {
            if (_utilisateurs == null)
                _utilisateurs = _mapper.Map<List<UtilisateurDto>>(await _utilisateurRepo.Lister());
            OnPropertyChanged(nameof(Utilisateurs));
        }
    }
}
