﻿using CommonServiceLocator;
using ProjetComplet.Dtos;
using ProjetComplet.VuesModeles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjetComplet.Vues
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LocationsPage : ContentPage
    {
        public LocationsPage()
        {
            InitializeComponent();

            BindingContext = ServiceLocator.Current.GetInstance<LocationsVueModele>();
        }

        protected override void OnAppearing()
        {
            var vm = (LocationsVueModele)BindingContext;

            vm.ChargerListeCommand.Execute(null);

            base.OnAppearing();
        }

        private async void LocationsListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
                return;

            await Navigation.PushAsync(new LocationDetailPage((LocationDto)e.SelectedItem));

            LocationsListView.SelectedItem = null;
        }


        private async void Bouton_Supprimer_Clic(object sender, EventArgs e)
        {
            await DisplayAlert("suppression", "La location a été supprimé avec succès!", "ok");
            await Navigation.PopAsync();
        }
    }
}