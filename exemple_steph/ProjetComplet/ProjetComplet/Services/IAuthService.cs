﻿using ProjetComplet.Modeles;
using System.Threading.Tasks;

namespace ProjetComplet.Services
{
    public interface IAuthService
    {
        Task<bool> Connexion(string nomUtilisateur, string motDePasse);
        void Deconnexion();
        Task<bool> EstConnecte();
        Task<int> GetIdUtilisateurConnecte();
        Task<string> GetNomUtilisateurConnecte();
        Task<string> GetRoleUtilisateurConnecte();
        Task<Utilisateur> GetUtilisateurConnecte();
    }
}
