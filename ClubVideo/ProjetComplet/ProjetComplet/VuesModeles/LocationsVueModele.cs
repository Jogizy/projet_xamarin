﻿using AutoMapper;
using ProjetComplet.Dtos;
using ProjetComplet.Modeles;
using ProjetComplet.Repositories;
using ProjetComplet.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace ProjetComplet.VuesModeles
{
    public class LocationsVueModele: BaseVueModele
    {

        private List<LocationDto> _locations;
        private LocationDto _locationSelectionne;
        private string _idCopieLocationRecherche;
        private bool _seulementEstRetourne;
        private readonly ILocationRepo _locationRepo;
        private readonly IMapper _mapper;


        public List<LocationDto> Locations
        {
            get => _locations?.Where(Filtrer).OrderBy(a => a.IdCopie).ToList();
        }

        public LocationDto LocationSelectionne { get => _locationSelectionne; set => SetValue(ref _locationSelectionne, value); }

        public string IdCopieLocationRecherche
        {
            get => _idCopieLocationRecherche;
            set
            {
                _idCopieLocationRecherche = value;
                OnPropertyChanged(nameof(Locations));
            }
        }

        /// Vérifie que la location est retournée
        public bool SeulementEstRetourne
        {
            get => _seulementEstRetourne;
            set
            {
                _seulementEstRetourne = value;
                OnPropertyChanged(nameof(Locations));
            }
        }

        public ICommand ChargerListeCommand { get; set; }
        public ICommand AjouterCommand { get; set; }
        public ICommand SupprimerCommand { get; set; }


        public LocationsVueModele(ILocationRepo locationRepo, IMapper mapper)
        {
            _locationRepo = locationRepo;
            _mapper = mapper;

            ChargerListeCommand = new Command(ChargerListe);
            AjouterCommand = new Command(Ajouter);
            SupprimerCommand = new Command<LocationDto>(Supprimer);
        }

        /// Supprimer la location dans la BD

        private async void Supprimer(LocationDto location)
        {
            var locationBd = await _locationRepo.Consulter(location.Id);
            await _locationRepo.Supprimer(locationBd);
            _locations.Remove(location);
            OnPropertyChanged(nameof(Locations));
        }


        /// Estime la date de retour prévue. Par défaut, 7 jours plus tard

        public DateTime Dateprevu()
        {
            int second, minute, hour, month, day, year;
            second = DateTime.Now.Second;
            minute = DateTime.Now.Minute;
            hour = DateTime.Now.Hour;
            month = DateTime.Now.Month;
            day = DateTime.Now.Day + 7;
            year = DateTime.Now.Year;

            if (day > 30)
            {
                day = 7 - (30 - DateTime.Now.Day);
                month = month + 1;
                if (month > 12)
                {
                    month = 1;
                    year = year + 1;
                }
            }
            DateTime s = new DateTime(year, month, day, hour, minute, second);
            return s;
        }

        /// Ajoute une location à la base de données
        private async void Ajouter()

        {
            var location = new Location
            {
                IdCopie = $"Nouveau location {DateTime.Now}",

                DateLocation = DateTime.Now,
                DateRetourPrevu = Dateprevu()
            };
            await _locationRepo.Ajouter(location);
            var locationDto = _mapper.Map<LocationDto>(location);
            _locations.Add(locationDto);
            OnPropertyChanged(nameof(Locations));
            LocationSelectionne = locationDto;
        }


        /// Charge la liste des locations

        private async void ChargerListe()
        {
            if (_locations == null)
                _locations = _mapper.Map<List<LocationDto>>(await _locationRepo.Lister());
            OnPropertyChanged(nameof(Locations));
        }


        /// Filtre les locations selon ce que l'utilisateur entre
        private bool Filtrer(LocationDto location)
        {

            if (!string.IsNullOrWhiteSpace(IdCopieLocationRecherche))
            {
                var elementsRecherches = IdCopieLocationRecherche.ToLower().Split(' ');
                foreach (var element in elementsRecherches)
                    if (!location.IdCopie.ToLower().Contains(element))
                        return false;
            }

            return true;
        }
    }
}
