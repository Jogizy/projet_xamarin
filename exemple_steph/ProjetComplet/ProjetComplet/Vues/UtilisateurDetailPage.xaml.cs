﻿using AutoMapper;
using CommonServiceLocator;
using ProjetComplet.Dtos;
using ProjetComplet.Modeles;
using ProjetComplet.Services;
using ProjetComplet.VuesModeles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjetComplet.Vues
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UtilisateurDetailPage : ContentPage
    {
        public UtilisateurDetailPage(UtilisateurDto utilisateur)
        {
            InitializeComponent();

            rolePicker.ItemsSource = Role.Liste;

            BindingContext = ServiceLocator.Current.GetInstance<IMapper>().Map(utilisateur,
                ServiceLocator.Current.GetInstance<UtilisateurDetailVueModele>());
        }

        private void ToolbarItem_Activated(object sender, EventArgs e)
        {
            var vm = (UtilisateurDetailVueModele)BindingContext;

            if (vm.SauvegarderCommand.CanExecute(null))
            {
                vm.SauvegarderCommand.Execute(null);
                Navigation.PopAsync();
            }
            else
                ServiceLocator.Current.GetInstance<IPageService>().AfficherMessageErreur(vm.MessageErreur);
        }
    }
}