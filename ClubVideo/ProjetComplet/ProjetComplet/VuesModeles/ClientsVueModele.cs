﻿using AutoMapper;
using ProjetComplet.Dtos;
using ProjetComplet.Modeles;
using ProjetComplet.Repositories;
using ProjetComplet.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;


namespace ProjetComplet.VuesModeles
{
    public class ClientsVueModele : BaseVueModele
    {
        private List<ClientDto> _clients;
        private ClientDto _clientSelectionne;
        private string _nomClientRecherche;
        private readonly IClientRepo _clientRepo;
        private readonly IMapper _mapper;
        private readonly IAuthService _authService;


        public List<ClientDto> Clients
        {
            get => _clients?.Where(Filtrer).OrderBy(a => a.Nom).ToList();
        }

        public ClientDto ClientSelectionne { get => _clientSelectionne; set => SetValue(ref _clientSelectionne, value); }


        /// Saisie le client recherché par nom

        public string NomClientRecherche
        {
            get => _nomClientRecherche;
            set
            {
                _nomClientRecherche = value;
                OnPropertyChanged(nameof(Clients));
            }
        }


        public ICommand ChargerListeCommand { get; set; }
        public ICommand AjouterCommand { get; set; }
        public ICommand SupprimerCommand { get; set; }



        public ClientsVueModele(IClientRepo clientRepo, IMapper mapper, IAuthService authService)
        {
            _clientRepo = clientRepo;
            _mapper = mapper;
            _authService = authService;

            ChargerListeCommand = new Command(ChargerListe);
            AjouterCommand = new Command(Ajouter);
            SupprimerCommand = new Command<ClientDto>(Supprimer);
        }


        /// Gère la suppression d'un client
        private async void Supprimer(ClientDto client)
        {
            var clientBd = await _clientRepo.Consulter(client.Id);
            await _clientRepo.Supprimer(clientBd);
            _clients.Remove(client);
            OnPropertyChanged(nameof(Clients));
        }

        /// Gère l'ajout d'un client
        private async void Ajouter()
        {
            var client = new Client
            {
                Nom = $"Nouveau client {DateTime.Now}"
            };
            await _clientRepo.Ajouter(client);
            var clientDto = _mapper.Map<ClientDto>(client);
            _clients.Add(clientDto);
            OnPropertyChanged(nameof(Clients));
            ClientSelectionne = clientDto;
        }


        /// Charge la liste de client pour l'afficher

        private async void ChargerListe()
        {
            if (_clients == null)
                _clients = _mapper.Map<List<ClientDto>>(await _clientRepo.Lister());
            OnPropertyChanged(nameof(Clients));
        }



        /// Applique le filtre de la recherche de client
        private bool Filtrer(ClientDto client)
        {
            if (!string.IsNullOrWhiteSpace(NomClientRecherche))
            {
                var elementsRecherches = NomClientRecherche.ToLower().Split(' ');
                foreach (var element in elementsRecherches)
                    if (!client.Nom.ToLower().Contains(element))
                        return false;
            }

            return true;
        }
    }
}