﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using AutoMapper;
using ProjetComplet.Dtos;
using ProjetComplet.Repositories;
using ProjetComplet.Modeles;
using ProjetComplet.Services;
using Xamarin.Forms;
using System.Linq;

namespace ProjetComplet.VuesModeles
{
    public class LocationDetailVueModele : BaseVueModele
    {
        private int _id;
        private string _client;
        private string _film;
        private string _datelocation;
        private string _dateretourprevu;
        private string _dateretour;
        private string _idcopie;
        private bool _estRetourne;

        public readonly IMapper _mapper;
        private readonly ILocationRepo _locationRepo;
        private readonly IClientRepo _clientRepo;
        private readonly IFilmRepo _filmRepo;



        public int Id { get => _id; set => SetValue(ref _id, value); }
        public string ClientLocation { get => _client; set => SetValue(ref _client, value); }
        public string FilmLocation { get => _film; set => SetValue(ref _film, value); }
        public string DateLocation { get => _datelocation; set => SetValue(ref _datelocation, value); }
        public string DateRetourPrevu { get => _dateretourprevu; set => SetValue(ref _dateretourprevu, value); }
        public string DateRetour { get => _dateretour; set => SetValue(ref _dateretour, value); }
        public string IdCopie { get => _idcopie; set => SetValue(ref _idcopie, value); }

        public bool Retour { get => _estRetourne; set => SetValue(ref _estRetourne, value); }

        public LocationDto Location { get; set; }



        public ICommand SauvegarderCommand { get; set; }


        public LocationDetailVueModele(IMapper mapper, ILocationRepo locationRepo, IFilmRepo filmRepo, IClientRepo clientRepo)
        {
            _mapper = mapper;
            _locationRepo = locationRepo;
            _filmRepo = filmRepo;
            _clientRepo = clientRepo;

            SauvegarderCommand = new Command(Sauvegarder, PeutSauvegarder);

        }



       /// Retourne l'information pour vérifier si il peut sauvegarder
        private bool PeutSauvegarder()
        {
            return !string.IsNullOrWhiteSpace(IdCopie);
        }


        /// Sauvegarde dans la base de données
        private async void Sauvegarder()
        {

            _mapper.Map(this, Location);

            var locationBd = await _locationRepo.Consulter(Id);
            _mapper.Map(Location, locationBd);

            await _locationRepo.Modifier(locationBd);
        }
    }
}
