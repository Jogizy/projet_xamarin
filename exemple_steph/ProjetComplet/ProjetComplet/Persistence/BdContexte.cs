﻿using Microsoft.EntityFrameworkCore;
using ProjetComplet.Modeles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ProjetComplet.Persistence
{
    public class BdContexte : DbContext, IPersistence
    {
        public DbSet<Utilisateur> Utilisateurs { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<ArticlePrefere> ArticlesPreferes { get; set; }

        public async Task<T> Ajouter<T>(T item) where T : class
        {
            return (await AddAsync(item)).Entity;
        }

        public Task<T> Consulter<T>(int id) where T : class
        {
            return FindAsync<T>(id);
        }

        public Task<T> Consulter<T>(Expression<Func<T, bool>> filtre) where T : class
        {
            return Set<T>().Where(filtre).FirstOrDefaultAsync();
        }

        public DbSet<T> Ensemble<T>() where T : class
        {
            return Set<T>();
        }

        public Task Init()
        {
            return Database.MigrateAsync();
        }

        public Task<List<T>> Lister<T>() where T : class
        {
            return Set<T>().ToListAsync();
        }

        public T Modifier<T>(T item) where T : class
        {
            return Update(item).Entity;
        }

        public Task<int> Sauvegarder()
        {
            return SaveChangesAsync();
        }

        public T Supprimer<T>(T item) where T : class
        {
            return Remove(item).Entity;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlite($"Filename={Constantes.CheminBd}");
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<ArticlePrefere>()
                .HasKey(ap => new { ap.UtilisateurId, ap.ArticleId });

            builder.Entity<ArticlePrefere>()
                .HasOne(ap => ap.Utilisateur)
                .WithMany(u => u.ArticlesPreferes)
                .HasForeignKey(ap => ap.UtilisateurId);
            builder.Entity<ArticlePrefere>()
                .HasOne(ap => ap.Article)
                .WithMany(u => u.UtilisateursArticlePrefere)
                .HasForeignKey(ap => ap.ArticleId);
        }
    }
}
