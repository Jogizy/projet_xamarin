﻿using CommonServiceLocator;
using ProjetComplet.Persistence;
using ProjetComplet.Services;
using ProjetComplet.Vues;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjetComplet
{
    public partial class App : Application
    {
        public App()
        {
            Bootstrapper.Initialize();

            InitializeComponent();

            MainPage = new ConnexionPage();
        }

        protected async override void OnStart()
        {
            await ServiceLocator.Current.GetInstance<IPersistence>().Init();
            //ServiceLocator.Current.GetInstance<IAuthService>().Deconnexion();
            if (await ServiceLocator.Current.GetInstance<IAuthService>().EstConnecte())
                MainPage = new MainPage();
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
