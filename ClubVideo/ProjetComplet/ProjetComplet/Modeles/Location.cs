﻿using System;
using System.Collections.Generic;
using System.Text;


namespace ProjetComplet.Modeles
{

    /// <summary>
    ///     La classe Location et ses attributs
    /// </summary>
    public class Location
    {

        public int Id { get; set; }
        public Client Client { get; set; }
        public Film Film { get; set; }
        public int? ClientId { get; set; }
        public int? FilmId { get; set; }
        public bool Retour { get; set; }
        public DateTime DateLocation { get; set; } 
        public DateTime DateRetourPrevu { get; set; }
        public DateTime DateRetour { get; set; }
        public string IdCopie { get; set; }
    }
}