﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using ProjetComplet.Modeles;
using AutoMapper;
using ProjetComplet.Dtos;
using ProjetComplet.Repositories;
using Xamarin.Forms;

namespace ProjetComplet.VuesModeles
{
    public class FilmDetailVueModele : BaseVueModele
    {
        private int _id;
        private string _nom;
        private string _categorie;
        private string _duree;
        private string _datedesortie;
        private string _description;
        private string _photo;
        private string _bandeannonce;
        private int _quantitetotale;
        private int _quantitedisponible;
        private decimal _prix;

        private readonly IMapper _mapper;
        private readonly IFilmRepo _filmRepo;

        public int Id { get => _id; set => SetValue(ref _id, value); }
        public string Nom { get => _nom; set => SetValue(ref _nom, value); }
        public string Categorie { get => _categorie; set => SetValue(ref _categorie, value); }
        public string Duree { get => _duree; set => SetValue(ref _duree, value); }
        public string DateDeSortie { get => _datedesortie; set => SetValue(ref _datedesortie, value); }
        public string Description { get => _description; set => SetValue(ref _description, value); }
        public string Photo { get => _photo; set => SetValue(ref _photo, value); }
        public string BandeAnnonce { get => _bandeannonce; set => SetValue(ref _bandeannonce, value); }
        public int QuantiteTotale { get => _quantitetotale; set => SetValue(ref _quantitetotale, value); }
        public int QuantiteDisponible { get => _quantitedisponible; set => SetValue(ref _quantitedisponible, value); }
        public decimal PrixLocation { get => _prix; set => SetValue(ref _prix, value); }

        public FilmDto Film { get; set; }

        public ICommand SauvegarderCommand { get; set; }

        public FilmDetailVueModele(IMapper mapper, IFilmRepo filmRepo)
        {
            _mapper = mapper;
            _filmRepo = filmRepo;
            SauvegarderCommand = new Command(Sauvegarder, PeutSauvegarder);
        }


        // Retourne l'information du compte qui peut sauvegarder

        private bool PeutSauvegarder()
        {
            return !string.IsNullOrWhiteSpace(Nom);
        }


        // Sauvegarde dans la base de données

        private async void Sauvegarder()
        {

            var film = new Film
            {
                Nom = $"Nouveau film {DateTime.Now}",

            };

            _mapper.Map(this, Film);

            var filmBd = await _filmRepo.Consulter(Id);
            _mapper.Map(Film, filmBd);

            await _filmRepo.Modifier(filmBd);
        }

    }
}
