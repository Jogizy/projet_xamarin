﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjetComplet.Modeles;
using ProjetComplet.Persistence;
using ProjetComplet.Services;

namespace ProjetComplet.Repositories
{

    /// <summary>
    ///     Classe qui permet de gérer les différentes sources pour le CRUD des clients
    /// </summary>

    public class ClientRepo : IClientRepo
    {
        private readonly IPersistence _bd;
        private readonly IAuthService _authService;



        /// <summary>
        ///     Constructeur de la classe 
        /// </summary>
        /// <param name="bd">définit ce que doit contenir le BdContexte </param>
        /// <param name="authService">authentifie l'utilisateur associé à la gestion des clients </param>

        public ClientRepo(IPersistence bd, IAuthService authService)
        {
            _bd = bd;
            _authService = authService;
        }



        /// <summary>
        ///     Tâche permettant l'ajout d'un client
        /// </summary>
        /// <param name="client">Client à ajouter</param>
        /// <returns></returns>


        public async Task<int> Ajouter(Client client)
        {
            await _bd.Ajouter(client);

            return await _bd.Sauvegarder();
        }


        /// <summary>
        ///     Tâche permettant la consultaion d'un client
        /// </summary>
        /// <param name="id"> identifiant du client à consuter</param>
        /// <returns> Informations sur le client</returns>

        public Task<Client> Consulter(int id)
        {
            return _bd.Ensemble<Client>().SingleOrDefaultAsync(a => a.Id == id);
        }


        /// <summary>
        ///     Tâche permettant d'obtenir la liste des clients 
        /// </summary>
        /// <returns>¸Liste de clients</returns>

        public Task<List<Client>> Lister()
        {
            return _bd.Ensemble<Client>().ToListAsync();
        }



        /// <summary>
        ///     Tâche permettant la modification du client
        /// </summary>
        /// <param name="client"> client à modifier </param>
        /// <returns>client modifié</returns>

        public async Task<int> Modifier(Client client)
        {
            _bd.Modifier(client);

            return await _bd.Sauvegarder();
        }


        /// <summary>
        ///     Tâche permettant la suppression du client
        /// </summary>
        /// <param name="client"> client à supprimer </param>
        /// <returns>client supprimé de la base de donnée</returns>

        public async Task<int> Supprimer(Client client)
        {
            _bd.Supprimer(client);

            return await _bd.Sauvegarder();
        }
    }
}