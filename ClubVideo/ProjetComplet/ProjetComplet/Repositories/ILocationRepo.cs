﻿using ProjetComplet.Modeles;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjetComplet.Repositories
{
    public interface ILocationRepo
    {
        Task<int> Ajouter(Location location);
        Task<Location> Consulter(int id);
        Task<List<Location>> Lister();
        Task<int> Modifier(Location location);
        Task<int> Supprimer(Location location);
    }
}
