﻿using AutoMapper;
using ProjetComplet.Modeles;
using ProjetComplet.Services;
using ProjetComplet.VuesModeles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjetComplet.Dtos
{
    public class AutoMapperProfils : Profile
    {
        public AutoMapperProfils(IAuthService authService)
        {
            CreateMap<Utilisateur, UtilisateurDto>();
            CreateMap<UtilisateurDto, Utilisateur>()
                .AfterMap((src, dest) => dest.SetMotDePasse(src.MotDePasse));
            CreateMap<UtilisateurDto, UtilisateurDetailVueModele>()
                .AfterMap((src, dest) => dest.Utilisateur = src);
            CreateMap<UtilisateurDetailVueModele, UtilisateurDto>();

            CreateMap<Article, ArticleDto>()
                .ForMember(dest => dest.EstPrefere, opt => opt
                    .MapFrom(src => src.UtilisateursArticlePrefere
                        .Any(ap => authService.GetIdUtilisateurConnecte().Result == ap.UtilisateurId)));
            CreateMap<ArticleDto, Article>();
            CreateMap<ArticleDto, ArticleDetailVueModele>()
                .AfterMap((src, dest) => dest.Article = src);
            CreateMap<ArticleDetailVueModele, ArticleDto>();
        }
    }
}
